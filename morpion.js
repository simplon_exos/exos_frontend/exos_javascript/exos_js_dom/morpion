let board = document.querySelector('#game-board');
let boxes = document.querySelectorAll('div');
let boxtest1 = document.querySelector('.box-1');
let compteur = 0

// 1. Faire apparaitre une nvlle div avec la classe player
// dans la div cliquée...

// 2. A faire sur toutes les div

// 3. Mettre un compteur pour changer la forme en fonction des tours

// 4. empecher d'ajouter plusieurs formes dans une même case

// 5. quand compteur = 9 la partie se termine

// 6. quand 3 formes sont alignées il y a un gagnant:
// On veut vérifier si la classe newShape cross ou round est présente
// dans 3 boxes sur la même ligne
// dans 3 boxes sur la même colonne
// dans 3 boses sur la diagonale
// PS: trouver les index dans notre tableau de boxes HTML



for (let i = 0; i < boxes.length; i++) {
    boxes[i].addEventListener('click', function() {
        // Si toutes les cases sont pleines, on stoppe la partie (5.)
        if (compteur === 9) {
            alert('La Partie est terminée !');
            document.location.reload();
        }
        //On crée la div qui contiendra la forme
        let newShape = document.createElement('div');

        if (!boxes[i].hasChildNodes()) {
            //On crée la div qui contiendra la forme
            let newShape = document.createElement('div');
            if (compteur % 2 === 0) {
                newShape.classList.add('playerX');
                boxes[i].classList.add('played');
                boxes[i].appendChild(newShape);
            } else {
                newShape.classList.add('playerO');
                boxes[i].classList.add('played');
                boxes[i].appendChild(newShape);
            }
            // on veut incrémenter le compteur de tour de jeu à chaque click (3.)
            compteur++;
        } else {
            // et bloquer les doublons (4.)
            alert('Cette case a déjà été jouée, essaye une autre !')
        }

        isWinner(boxes);
    });
}

// (.6)

function isWinner(array) {
    // Gain sur la 1ere ligne avec les croix :
    if (array[0].classList.contains('played') &&
        array[1].classList.contains('played') &&
        array[2].classList.contains('played')) {
        if (array[0].hasChildNodes && array[0].firstChild.classList.contains('playerX') &&
            array[1].hasChildNodes && array[1].firstChild.classList.contains('playerX') &&
            array[2].hasChildNodes && array[2].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 1ere ligne avec les rounds :
        if (array[0].hasChildNodes && array[0].firstChild.classList.contains('playerO') &&
            array[1].hasChildNodes && array[1].firstChild.classList.contains('playerO') &&
            array[2].hasChildNodes && array[2].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }

    // Gain sur la 2e ligne avec les croix :
    if (array[3].classList.contains('played') &&
        array[4].classList.contains('played') &&
        array[5].classList.contains('played')) {
        if (array[3].hasChildNodes && array[3].firstChild.classList.contains('playerX') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerX') &&
            array[5].hasChildNodes && array[5].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 2e ligne avec les rounds :
        if (array[3].hasChildNodes && array[3].firstChild.classList.contains('playerO') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerO') &&
            array[5].hasChildNodes && array[5].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }

    // Gain sur la 3e ligne avec les croix :
    if (array[6].classList.contains('played') &&
        array[7].classList.contains('played') &&
        array[8].classList.contains('played')) {
        if (array[6].hasChildNodes && array[6].firstChild.classList.contains('playerX') &&
            array[7].hasChildNodes && array[7].firstChild.classList.contains('playerX') &&
            array[8].hasChildNodes && array[8].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 3e ligne avec les rounds :
        if (array[6].hasChildNodes && array[6].firstChild.classList.contains('playerO') &&
            array[7].hasChildNodes && array[7].firstChild.classList.contains('playerO') &&
            array[8].hasChildNodes && array[8].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }
    // Gain sur la 1ere colonne avec les croix :
    if (array[0].classList.contains('played') &&
        array[3].classList.contains('played') &&
        array[6].classList.contains('played')) {
        if (array[0].hasChildNodes && array[0].firstChild.classList.contains('playerX') &&
            array[3].hasChildNodes && array[3].firstChild.classList.contains('playerX') &&
            array[6].hasChildNodes && array[6].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 1ere colonne avec les rounds :
        if (array[0].hasChildNodes && array[0].firstChild.classList.contains('playerO') &&
            array[3].hasChildNodes && array[3].firstChild.classList.contains('playerO') &&
            array[6].hasChildNodes && array[6].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }
    // Gain sur la 2e colonne avec les croix :
    if (array[1].classList.contains('played') &&
        array[4].classList.contains('played') &&
        array[7].classList.contains('played')) {
        if (array[1].hasChildNodes && array[1].firstChild.classList.contains('playerX') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerX') &&
            array[7].hasChildNodes && array[7].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 2e colonne avec les rounds :
        if (array[1].hasChildNodes && array[1].firstChild.classList.contains('playerO') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerO') &&
            array[7].hasChildNodes && array[7].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }
    // Gain sur la 3e colonne avec les croix :
    if (array[2].classList.contains('played') &&
        array[5].classList.contains('played') &&
        array[8].classList.contains('played')) {
        if (array[2].hasChildNodes && array[2].firstChild.classList.contains('playerX') &&
            array[5].hasChildNodes && array[5].firstChild.classList.contains('playerX') &&
            array[8].hasChildNodes && array[8].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 3e colonne avec les rounds :
        if (array[2].hasChildNodes && array[2].firstChild.classList.contains('playerO') &&
            array[5].hasChildNodes && array[5].firstChild.classList.contains('playerO') &&
            array[8].hasChildNodes && array[8].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }

    // Gain sur la 1ere diagonale (de haut gauche à bas droite) avec les croix :
    if (array[0].classList.contains('played') &&
        array[4].classList.contains('played') &&
        array[8].classList.contains('played')) {
        if (array[0].hasChildNodes && array[0].firstChild.classList.contains('playerX') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerX') &&
            array[8].hasChildNodes && array[8].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 1ere diagonale  avec les rounds :
        if (array[0].hasChildNodes && array[0].firstChild.classList.contains('playerO') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerO') &&
            array[8].hasChildNodes && array[8].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }

    // Gain sur la 2e diagonale (de haut droite à bas gauche) avec les croix :
    if (array[2].classList.contains('played') &&
        array[4].classList.contains('played') &&
        array[6].classList.contains('played')) {
        if (array[2].hasChildNodes && array[2].firstChild.classList.contains('playerX') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerX') &&
            array[6].hasChildNodes && array[6].firstChild.classList.contains('playerX')) {
            alert('The cross Player wins !');
            document.location.reload();
        }
        // Gain sur la 2e diagonale  avec les rounds :
        if (array[2].hasChildNodes && array[2].firstChild.classList.contains('playerO') &&
            array[4].hasChildNodes && array[4].firstChild.classList.contains('playerO') &&
            array[6].hasChildNodes && array[6].firstChild.classList.contains('playerO')) {
            alert('The round Player wins !');
            document.location.reload();
        }
    }
};